package oculus.tablist.bungee;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;
import net.md_5.bungee.event.EventHandler;
import oculus.tablist.bungee.apis.Config;
import oculus.tablist.utils.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class BungeeMain extends Plugin implements Listener {

    private static BungeeMain intance = null;

    public static String format(String str, ProxiedPlayer p) {
        Date date = new Date();

        if (p.getServer() != null) {
            str = str.replace("{SERVER}",p.getServer().getInfo().getName());
        }

        return ChatColor.translateAlternateColorCodes('&',str)
                .replace("{TIME}", new SimpleDateFormat("HH:mm:ss").format(date))
                .replace("{DATE}", new SimpleDateFormat("dd.MM.yyyy").format(date))
                .replace("{PLAYERCOUNT}", String.valueOf(ProxyServer.getInstance().getOnlineCount()))
                .replace("{MAXPLAYERS}",String.valueOf(ProxyServer.getInstance().getConfig().getPlayerLimit()));
    }

    public static void updatePlayerListHeaderFooter() {
        for (ProxiedPlayer p : ProxyServer.getInstance().getPlayers()) {
            p.setTabHeader(
                    TextComponent.fromLegacyText(format(StringUtils.join("§r\n", Config.Configuration().getStringList("Tablist.Header")),p)),
                    TextComponent.fromLegacyText(format(StringUtils.join("§r\n", Config.Configuration().getStringList("Tablist.Footer")),p))
            );
        }
    }

    public static BungeeMain getInstance() {
        return BungeeMain.intance;
    }

    @Override
    public void onEnable() {
        intance = this;
        new Metrics(this, 11544);
        Config.setup();
        PluginManager pluginManager = ProxyServer.getInstance().getPluginManager();
        pluginManager.registerListener(this, this);
        ProxyServer.getInstance().getScheduler().schedule(this, BungeeMain::updatePlayerListHeaderFooter, 0, 1, TimeUnit.SECONDS);
    }

    @EventHandler
    public void onJoin(PostLoginEvent e) {
        updatePlayerListHeaderFooter();
    }

}
